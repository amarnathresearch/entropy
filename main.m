a = imread('input.jpg');
level = graythresh(a);
a = im2bw(a,level);
figure, imshow(a);
[h, w] = size(a);

ent = zeros(h, 1);

for i=1:h
    c = 0;
    for j=1:w-1
        if a(i, j)~=a(i, j+1)
            c = c + 1;
        end
    end
    ent(i, 1) = c;
end
s = 0;
e = -1;
for i=1:h-1
    if ent(i, 1) > 0 && ent(i+1, 1) == 0
        s = i+1;
    elseif ent(i, 1) == 0 && ent(i+1, 1) > 1 && s > e
        e = i;
        avg = ceil((e+s)/2);
        a(avg, :) = 0;
    end
end

figure, imshow(a);
imwrite(a, 'output.jpg');
return;

a = [0,  0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0;
0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0;
0,	0,	0,	0,	0,	0,	1,	1,	1,	1,	0,	0,	0,	0,	0,	0;
0,	0,	0,	0,	0,	1,	1,	0,	0,	1,	1,	0,	0,	0,	0,	0;
0,	0,	0,	0,	0,	1,	1,	0,	0,	1,	1,	0,	0,	0,	0,	0;
0,	0,	0,	0,	0,	1,	1,	1,	1,	1,	1,	0,	0,	0,	0,	0;
0,	0,	0,	0,	0,	1,	1,	1,	1,	1,	1,	0,	0,	0,	0,	0;
0,	0,	0,	0,	0,	1,	1,	0,	0,	1,	1,	0,	0,	0,	0,	0;
0,	0,	0,	0,	0,	1,	1,	0,	0,	1,	1,	0,	0,	0,	0,	0;
0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0;
0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0,	0];

a = imresize(a, 10);
level = graythresh(a);
a = im2bw(a,level);
figure, imshow(~a);
imwrite(~a, 'sample.jpg');